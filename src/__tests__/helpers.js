const { convertToArray, convertToNumber, convertToBoolean } = require('../helpers')

describe('convertToBoolean', () => {
  it.each([
    [undefined, undefined],
    [null, null],
    [true, true],
    [false, false],
    ['TRUE', true],
    [' TRUE ', true],
    ['true', true],
    ['t', true],
    ['f', false],
    ['false', false],
    [' false ', false],
    [1, true],
    [0, false],
  ])('should convert %s to %s', (val, expected) => {
    expect(convertToBoolean(val)).toBe(expected)
  })
})

describe('convertToNumber', () => {
  it.each([
    [undefined, NaN],
    [1, 1],
    [true, NaN],
    ['here', NaN],
    [null, NaN],
    [NaN, NaN],
    [false, NaN],
    ['hellobuddy', NaN],
    ['one1one', NaN],
    [[], NaN],
    [{}, NaN],
    [1, 1],
    [1.3, 1.3],
    [1.23432, 1.23432],
    ['1', 1],
    ['1.3', 1.3],
  ])('should convert %s to %s', (val, expected) => {
    expect(convertToNumber(val)).toBe(expected)
  })
})

describe('convertToArray', () => {
  it.each([
    [undefined, undefined],
    [null, null],
    [true, true],
    [['foo'], ['foo']],
    [['foo '], ['foo ']], // If already an array, returns original preserved
    ['foo', ['foo']],
    [' foo', ['foo']],
    ['foo,bar', ['foo', 'bar']],
    ['foo, bar', ['foo', 'bar']],
    [' foo , bar ', ['foo', 'bar']],
    [' foo fum , bar ', ['foo fum', 'bar']],
  ])('should convert %s to %s', (val, expected) => {
    expect(convertToArray(val)).toEqual(expected)
  })
})
