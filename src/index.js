const { convertToBoolean, convertToArray, getIniValues } = require('./helpers')
const pick = require('lodash.pick')

const handler = {
  get (target, objectKey) {
    // Sugar to write config.IS_COOL instead of config.get('IS_COOL')
    const prop = target[objectKey]
    return prop !== undefined ? prop : target.get(objectKey)
  },

  set (target, objectKey, value) {
    // Sugar to write config.IS_COOL = 'yes' instead of config.set('IS_COOL', 'yes')
    const prop = target[objectKey]
    if (prop !== undefined) {
      prop[objectKey] = value
      return
    }
    target.set(objectKey, value)
  },
}

class Config {
  constructor (defaults = {}, iniFilename) {
    this.defaults = { ...defaults }
    this.keys = Object.keys(defaults)
    this.store = { ...defaults }

    // Load values from init into store
    this.loadINI(iniFilename)

    // Load values from env into store
    this.loadEnv()
  }

  set (key, value) {
    // If Array, then convert string to array and store
    if (Array.isArray(this.defaults[key])) {
      this.store[key] = convertToArray(value)
      return
    }

    // If default is a boolean, then convert to boolean
    if (typeof this.defaults[key] === 'boolean') {
      this.store[key] = convertToBoolean(value)
      return
    }
    // If default is an integer
    if (typeof this.defaults[key] === 'number' && Number.isInteger(this.defaults[key])) {
      this.store[key] = parseInt(value, 10)
      return
    }
    // If default is a float
    if (typeof this.defaults[key] === 'number' && !Number.isInteger(this.defaults[key])) {
      this.store[key] = parseFloat(value)
      return
    }

    // From here it's just setting a string
    this.store[key] = value
  }

  get (key) {
    return this.store[key]
  }

  loadValues (vals = {}) {
    // When values are defined in environment variables they always come in as
    // strings, we need to convert them to their respective array/boolean/number values
    // Grabs all values in defaults as [[key, value], [key, value]]...
    // This enforces the same type defined in the defaults
    Object.entries(vals).forEach(([key, value]) => {
      this.set(key, value)
    })
  }

  loadINI (iniFilename) {
    const iniValues = getIniValues(process.cwd(), iniFilename)
    const values = pick(iniValues, this.keys)
    this.loadValues(values)
  }

  loadEnv () {
    const values = pick(process.env, this.keys)
    this.loadValues(values)
  }
}

module.exports = function (_defaults = {}, iniFileName) {
  const config = new Config(_defaults, iniFileName)
  return new Proxy(config, handler)
}
